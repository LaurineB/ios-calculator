//
//  SBrain.swift
//  SCalc
//
//  Created by Nico Zino on 2017/03/10.
//  Copyright © 2017 Nico Zino. All rights reserved.
//

import Foundation

func multiply(arg1 : Double, arg2: Double) -> Double {
    return arg1*arg2
}
func division(arg1 : Double, arg2 : Double) -> Double {
    if arg2 == 0 {
        return 0.0
    } else {
        return arg1 / arg2
    }
}
//--------------------------------------------------------------------------------
class SBrain {
    
    private var accumulator : Double?
    private var secondOperandSaved : Double?
    private var symbolSaved : String?
    
    public enum Symbol : String {
        case pi = "π"
        case squareRoot = "√"
        case multiply = "*"
        case division = "/"
        case substraction = "-"
        case addition = "+"
        case equal = "="
        case square = "²"
    }
    
    private enum OperationType {
        case constant(Double)
        case unaryOperation((Double)->Double)
        case binaryOperation((Double,Double)->Double)
        case equals
    }
    
    private var operations : Dictionary<String, OperationType> = [
        Symbol.pi.rawValue : OperationType.constant(Double.pi),
        Symbol.squareRoot.rawValue : OperationType.unaryOperation(sqrt), // ?
        Symbol.multiply.rawValue : OperationType.binaryOperation(multiply),
        Symbol.division.rawValue : OperationType.binaryOperation(division),
        Symbol.substraction.rawValue : OperationType.binaryOperation({ (x, y) -> Double in
            return x - y
        }),
        Symbol.addition.rawValue : OperationType.binaryOperation({ $0 + $1 }),
        Symbol.equal.rawValue : OperationType.equals,
        Symbol.square.rawValue : OperationType.unaryOperation({ $0 * $0})
    ]
    
    func performMath(symbol: String) {
        if let op = operations[symbol] {
            switch op {
            case .constant(let c):
                accumulator = c
            case .unaryOperation(let f):
                if accumulator != nil {
                    accumulator = f(accumulator!)
                }
            case .binaryOperation(let f):
                if accumulator != nil {
                    pendingBinary = PendingBinaryOperation(function: f, firstOperand: accumulator!, symbol: symbol ) //?
                    symbolSaved = symbol
                    accumulator = nil
                }
            case .equals:
                performPendingBinaryOp()
            default:
                break
            }
            
        }
        
    }
    
    private var pendingBinary : PendingBinaryOperation?
    
    private func performPendingBinaryOp() {
        
        if pendingBinary != nil && accumulator != nil {
            
            if(symbolSaved == Symbol.division.rawValue) {
                
                //première fois que l'on fait l'opération
                if(secondOperandSaved == nil) {
                    
                    secondOperandSaved = accumulator
                    accumulator = pendingBinary?.perform(with: accumulator!)
                } else {
                     accumulator = pendingBinary?.perform(with: secondOperandSaved!)
                }
                
            } else {
                accumulator = pendingBinary?.perform(with: accumulator!)
            }
        }
    }
    
    public func clearPendingBinaryOperation() {
        self.pendingBinary = nil
    }
    //-------------------------------------------------------------------
    private struct PendingBinaryOperation { //?
        let function : (Double,Double) -> Double //?
        var firstOperand : Double//?
        var symbol : String
        
        mutating func perform(with secondOperand : Double) -> Double {
            let result = function(firstOperand, secondOperand)
            switch symbol {
            case Symbol.division.rawValue:
                firstOperand = result
            
            case Symbol.multiply.rawValue:
                firstOperand = secondOperand
            default:
                break
            }
            
            return result
        }
    }
    //-------------------------------------------------------------------
    func setOperand(operand: Double?) {
        accumulator = operand
    }
    
    var result: Double {
        get {
            return accumulator ?? 0 
        }
    }
}
